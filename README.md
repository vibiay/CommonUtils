# CommonUtils

> javaScript 工作中收集的工具方法

- [ ] 数据校验
- [ ] 字符串操作
- [ ] 时间格式化
- [ ] 数据处理等
- [ ] 通用方法

> 使用只需要导入 `CommonUtils.js`即可

### [文档地址](https://zhuzhaofeng.gitee.io/CommonUtils/)